Para executar o projeto:

**Requisitos:**

- Java 11
- Maven 3.6

**Instalar/Executar:**

- cd <path>/npg1401-osapp
- mvn clean package install
- java -jar <path>/npg1401-osapp/target/osapp-1.0.0.jar

**Login/Senha:**

- bruno/estacio
- estacio/estacio