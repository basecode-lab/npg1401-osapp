package br.dev.brunocesar.estacio.osapp.context;

import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.google.common.hash.Hashing;

public final class PersistenceContext {
	
	public static final String PU = "npg1401-osapp";
	
	public static final String DB = "npg1401_osapp";

	private EntityManagerFactory entityManagerFactory;

	public PersistenceContext() {/**/}
	
	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}
	
	public EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
	
	public String hash(String str){
		return Hashing.sha256().hashString(str, StandardCharsets.UTF_8).toString();
	}
	
	@SuppressWarnings("unchecked")
	public boolean isDatabaseInstalled() {
		
		final List<String> resultList = entityManagerFactory.createEntityManager().createNativeQuery("show schemas").getResultList();
		
		return resultList.contains(PersistenceContext.DB.toUpperCase());
		
	}
	
}
