package br.dev.brunocesar.estacio.osapp.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.StringUtils;

import br.dev.brunocesar.estacio.osapp.entity.Cliente;
import br.dev.brunocesar.estacio.osapp.entity.OrdemServico;

public class OrdemServicoTableModel extends AbstractTableModel{

	private static final long serialVersionUID = 1L;
	
	private static final String[] colunas = new String[]{"NU. OS","DT. Cadastro", "Cliente","Descricao","Situacao"};

	private final transient List<OrdemServico> ordensServico;
	
	private final transient  Map<Integer, IntFunction<Object>> indexMethodMap = new HashMap<>();
	
	public OrdemServicoTableModel(List<OrdemServico> ordensServico) {
		this.ordensServico = ordensServico;
		initMap();
	}
	

	private void initMap() {
		indexMethodMap.put(0, this::getNumero);
		indexMethodMap.put(1, this::getDataCadastro);
		indexMethodMap.put(2, this::getCliente);
		indexMethodMap.put(3, this::getDescricao);
		indexMethodMap.put(4, this::getSituacao);
	}

	@Override
	public int getRowCount() {
		return ordensServico.size();
	}

	@Override
	public int getColumnCount() {
		return colunas.length;
	}

	private String getNumero(int index) {
		return StringUtils.leftPad(String.valueOf(ordensServico.get(index).getNumero()), 6, '0');
	}
	
	private String getDataCadastro(int index) {
		Date dataCadastro = ordensServico.get(index).getDataCadastro();
		return new SimpleDateFormat("dd/MM/yyyy").format(dataCadastro);
	}
	
	private String getCliente(int index) {
		Cliente cliente = ordensServico.get(index).getCliente();
		return cliente.getNome();
	}
	
	private String getDescricao(int index) {
		return ordensServico.get(index).getDescricao();
	}
	
	private String getSituacao(int index) {
		return ordensServico.get(index).getSituacao().getDescricao();
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		IntFunction<Object> fn = this.indexMethodMap.get(columnIndex);
		if(fn != null) return fn.apply(rowIndex);
		return null;
	}
	
	@Override
	public String getColumnName(int column) {
		return colunas[column];
	}
	
	public List<OrdemServico> getOrdensServico() {
		return ordensServico;
	}

}
