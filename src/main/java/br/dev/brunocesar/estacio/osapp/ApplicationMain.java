package br.dev.brunocesar.estacio.osapp;

import javax.swing.JFrame;

import br.dev.brunocesar.estacio.osapp.context.ApplicationContext;
import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;
import br.dev.brunocesar.estacio.osapp.context.ViewContext;
import br.dev.brunocesar.estacio.osapp.controller.MainController;
import br.dev.brunocesar.estacio.osapp.factory.ControllerFactory;

public final class ApplicationMain {
	
	public static void main(String[] args) {
		
		final ViewContext viewContext;
		
		final PersistenceContext persistenceContext;
		
		final ApplicationContext appContext;
		
		
		final ControllerFactory controllerFactory;
		
		final MainController mainController;
		
		
		viewContext =  new ViewContext(new JFrame());

		persistenceContext = new PersistenceContext();
		
		appContext = new ApplicationContext();
		
		
		controllerFactory = ControllerFactory.getInstance(viewContext, persistenceContext, appContext);
		
		
		mainController = controllerFactory.getMainController();

		mainController.startApplication();
		
	}
	
}
