package br.dev.brunocesar.estacio.osapp.infra;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import br.dev.brunocesar.estacio.osapp.context.ApplicationContext;

public class MessagesLoader {
	
	private final ApplicationContext context;
	
	public static final String FILE_NAME = "/messages.properties";

	public MessagesLoader(ApplicationContext context) {
		this.context = context;
	}
	
	public void load()  {

		final Properties messages = new Properties();
		
		try(InputStream is = MessagesLoader.class.getResourceAsStream(FILE_NAME)){
		
			messages.load(new InputStreamReader(is, StandardCharsets.UTF_8));
			
			context.setMessages(messages);
		
		}catch(Exception e) {

			throw new IllegalStateException(e);

		}
		
	}

}
