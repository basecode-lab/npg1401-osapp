package br.dev.brunocesar.estacio.osapp.factory;

import br.dev.brunocesar.estacio.osapp.context.ApplicationContext;
import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;
import br.dev.brunocesar.estacio.osapp.context.ViewContext;
import br.dev.brunocesar.estacio.osapp.controller.LoginController;
import br.dev.brunocesar.estacio.osapp.controller.MainController;
import br.dev.brunocesar.estacio.osapp.controller.OrdemServicoController;

public final class ControllerFactory {

	private final ViewContext viewContext;

	private final PersistenceContext persistenceContext;

	private static ControllerFactory factory;

	private final ApplicationContext appContext;

	private ControllerFactory(ViewContext viewContext, PersistenceContext persistenceContext, ApplicationContext applicationContext) {
		this.viewContext = viewContext;
		this.persistenceContext = persistenceContext;
		this.appContext = applicationContext;
	}

	public static ControllerFactory getInstance(ViewContext viewContext, PersistenceContext persistenceContext,
			ApplicationContext appContext) {
		if(factory == null) factory = new ControllerFactory(viewContext, persistenceContext, appContext);
		return factory;
	}

	public static ControllerFactory getInstance() {
		return factory;
	}

	public LoginController getLoginController() {
		return new LoginController(viewContext, persistenceContext, appContext);
	}

	public MainController getMainController() {
		return new MainController(viewContext, persistenceContext, appContext);
	}

	public OrdemServicoController getOrdemServicoController() {
		return new OrdemServicoController(viewContext, persistenceContext, appContext);
	}

}
