package br.dev.brunocesar.estacio.osapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;
import br.dev.brunocesar.estacio.osapp.status.SituacaoOrdemServico;

@Entity(name="OrdemServico")
@Table(schema=PersistenceContext.DB, name="ordem_servico")
public class OrdemServico {
	
	@Id
	@Column(name="nu_ordem_servico")
	private Long numero;
	
	@Column(name="dt_cadastro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name="ds_login")
	private String login;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_cliente")
	private Cliente cliente;
	
	@Column(name="tx_ordem_servico")
	private String descricao;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="st_ordem_servico")
	private SituacaoOrdemServico situacao;

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public SituacaoOrdemServico getSituacao() {
		return situacao;
	}
	
	public void setSituacao(SituacaoOrdemServico situacao) {
		this.situacao = situacao;
	}
	
}
