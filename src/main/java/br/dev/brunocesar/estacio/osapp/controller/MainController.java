package br.dev.brunocesar.estacio.osapp.controller;

import java.io.IOException;

import br.dev.brunocesar.estacio.osapp.context.ApplicationContext;
import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;
import br.dev.brunocesar.estacio.osapp.context.ViewContext;
import br.dev.brunocesar.estacio.osapp.infra.DatabaseInstaller;
import br.dev.brunocesar.estacio.osapp.infra.MessagesLoader;
import br.dev.brunocesar.estacio.osapp.infra.PersistenceInitializer;

public class MainController extends Controller{

	public MainController(ViewContext viewContext, PersistenceContext persistenceContext, ApplicationContext appContext) {
		super(viewContext, persistenceContext, appContext);
	}

	public void startApplication() {

		try {

			initMessageResources();

			initPersistenceContext();

			checkAndInstallDatabase();

			startUI();

		}catch(Exception e) {

			showError(getMessage("app.init.error"), e.getMessage());

		}

	}


	private void initMessageResources() {

		new MessagesLoader(getAppContext()).load();

	}

	private void initPersistenceContext() throws IOException {

		new PersistenceInitializer(getPersistenceContext()).initialize();


	}

	private void checkAndInstallDatabase() throws IOException {


		if(!getPersistenceContext().isDatabaseInstalled()) {

			new DatabaseInstaller(getPersistenceContext()).install();

		}

	}

	private void startUI() {
		getViewContext().openLoginView();		
	}
}