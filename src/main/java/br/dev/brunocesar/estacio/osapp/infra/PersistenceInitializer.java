package br.dev.brunocesar.estacio.osapp.infra;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;
import br.dev.brunocesar.estacio.osapp.controller.MainController;

public class PersistenceInitializer {

	private PersistenceContext persistenceContext;

	private static final String PROPERTIES_FILE = "/h2.properties";
	
	public PersistenceInitializer(PersistenceContext persistenceContext) {
		super();
		this.persistenceContext = persistenceContext;
	}
	
	
	public void initialize() throws IOException {
		
		Properties properties = new Properties();
		
		properties.load(MainController.class.getResourceAsStream(PROPERTIES_FILE));
		
		final EntityManagerFactory factory = Persistence.createEntityManagerFactory(PersistenceContext.PU, properties);
		
		persistenceContext.setEntityManagerFactory(factory);
		
	}
	
	
}
