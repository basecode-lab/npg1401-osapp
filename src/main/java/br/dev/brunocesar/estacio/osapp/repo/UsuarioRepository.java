package br.dev.brunocesar.estacio.osapp.repo;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.dev.brunocesar.estacio.osapp.entity.Usuario;

public class UsuarioRepository extends AbstractRepository{

	public UsuarioRepository(EntityManager entityManager) {
		super(entityManager);
	}

	public Usuario getByIdAndPassword(String login, String senha) {

		try {

			TypedQuery<Usuario> query = getEntityManager().createQuery("from Usuario where login = :login and senha = :senha", Usuario.class);
			
			query.setParameter("login", login);
			
			query.setParameter("senha", senha);
			
			return query.getSingleResult();
			
		}catch(Exception e) {
			
			return null;
			
		}
		

	}


}
