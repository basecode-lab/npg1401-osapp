package br.dev.brunocesar.estacio.osapp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;

@Entity(name="Cliente")
@Table(schema=PersistenceContext.DB, name="cliente")
public class Cliente implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_cliente")
	private Long id;
	
	@Column(name="no_cliente")
	private String nome;
	
	@Column(name="nu_telefone")
	private String numeroTelefone;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_cadastro")
	private Date dataCadastro;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumeroTelefone() {
		return numeroTelefone;
	}

	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}
	
	public Date getDataCadastro() {
		return dataCadastro;
	}
	
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
}
