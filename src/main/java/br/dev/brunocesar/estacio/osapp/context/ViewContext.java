package br.dev.brunocesar.estacio.osapp.context;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import br.dev.brunocesar.estacio.osapp.factory.ControllerFactory;
import br.dev.brunocesar.estacio.osapp.view.LoginView;
import br.dev.brunocesar.estacio.osapp.view.OrdemServicoView;

public class ViewContext {
	
	private final JFrame mainFrame;


	public ViewContext(JFrame mainFrame) {
		
		this.mainFrame = mainFrame;
		
	}
	

	public void openLoginView() {
		
		mainFrame.setContentPane(new LoginView(ControllerFactory.getInstance().getLoginController()));
		
		mainFrame.setSize(800,600);

		mainFrame.setVisible(true);

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		mainFrame.setLocation(dim.width/2-mainFrame.getSize().width/2, dim.height/2-mainFrame.getSize().height/2);
		
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	
	public void openOrdemServicoView() {
		
		mainFrame.setContentPane(new OrdemServicoView(ControllerFactory.getInstance().getOrdemServicoController()));
		
		mainFrame.setVisible(true);
		
	}
	
	public void showErrorMessage(Component component, String title, String message) {
		
		JOptionPane.showMessageDialog(component, message, title, JOptionPane.ERROR_MESSAGE);
		
	}
	
	public void showInfoMessage(Component component, String title, String message) {
		
		JOptionPane.showMessageDialog(component, message, title, JOptionPane.INFORMATION_MESSAGE);
		
	}
	
}
