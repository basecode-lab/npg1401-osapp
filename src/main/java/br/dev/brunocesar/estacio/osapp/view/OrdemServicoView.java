package br.dev.brunocesar.estacio.osapp.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;

import br.dev.brunocesar.estacio.osapp.controller.OrdemServicoController;
import br.dev.brunocesar.estacio.osapp.model.OrdemServicoTableModel;

public class OrdemServicoView extends JPanel{

	private static final long serialVersionUID = 1L;

	private final transient OrdemServicoController controller;

	private JTable ordensAtivasTable;

	private JTable ordensFinalizadasTable;

	public OrdemServicoView(OrdemServicoController controller) {
		this.controller = controller;
		initLayout();
		initActions();
	}

	private void initActions() {

		JPopupMenu popMenu = new JPopupMenu();

		JMenuItem item = new JMenuItem("Finalizar");

		item.addActionListener(this::finalizar);

		popMenu.add(item);

		ordensAtivasTable.setComponentPopupMenu(popMenu);

		ordensAtivasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		
		popMenu = new JPopupMenu();

		item = new JMenuItem("Reabrir");

		item.addActionListener(this::reabrir);

		popMenu.add(item);

		ordensFinalizadasTable.setComponentPopupMenu(popMenu);

		ordensFinalizadasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);


	}

	private void initLayout() {
		
		setLayout(new GridLayout(1, 2));

		setBackground(Color.WHITE);
		
		ordensAtivasTable = new JTable(controller.getOrdensAtivasTableModel());

		JScrollPane scrollpane = new JScrollPane(ordensAtivasTable);

		TitledBorder titledBorder = new TitledBorder("ATIVAS");

		scrollpane.setBorder(titledBorder);
		
		add(scrollpane);
		
		ordensFinalizadasTable = new JTable(controller.getOrdensFinalizadasTableModel());

		scrollpane = new JScrollPane(ordensFinalizadasTable);

		titledBorder = new TitledBorder("INATIVAS");

		scrollpane.setBorder(titledBorder);
		
		add(scrollpane);

	}

	private void atualizarTabelas() {
		
		ordensAtivasTable.setModel(controller.getOrdensAtivasTableModel());
		
		ordensFinalizadasTable.setModel(controller.getOrdensFinalizadasTableModel());
		
	}
	
	private void validateRow(int row) {
		if(row < 0) throw new IllegalArgumentException("selecione uma ordem de servico");
	}
	
	

	private void reabrir(ActionEvent evt) {

		try {
			
			int selectedRow = ordensFinalizadasTable.getSelectedRow();
			
			validateRow(selectedRow);
			
			OrdemServicoTableModel model = (OrdemServicoTableModel) ordensFinalizadasTable.getModel();
			
			controller.reabrir(model.getOrdensServico().get(selectedRow).getNumero());

			atualizarTabelas();
			
		}catch(Exception e) {
			
			controller.getViewContext().showErrorMessage(this, controller.getMessage("order.reopen.error"), e.getMessage());
			
		}
		
		
	}

	private void finalizar(ActionEvent evt) {

		try {
			
			int selectedRow = ordensAtivasTable.getSelectedRow();
			
			validateRow(selectedRow);
			
			OrdemServicoTableModel model = (OrdemServicoTableModel) ordensAtivasTable.getModel();
			
			controller.finalizar(model.getOrdensServico().get(selectedRow).getNumero());

			atualizarTabelas();
			
		}catch(Exception e) {
			
			controller.getViewContext().showErrorMessage(this, controller.getMessage("order.done.error"), e.getMessage());
			
		}

	}

}
