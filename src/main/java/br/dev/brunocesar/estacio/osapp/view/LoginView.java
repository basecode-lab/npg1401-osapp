package br.dev.brunocesar.estacio.osapp.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import br.dev.brunocesar.estacio.osapp.controller.LoginController;
import br.dev.brunocesar.estacio.osapp.model.LoginModel;

public class LoginView extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private static final int FIELDS_SIZE = 10;
	
	private JTextField userField;
	
	private JPasswordField passField;
	
	private JButton loginButton;
	
	private final transient LoginController controller;
	
	public LoginView(LoginController controller) {
		this.controller = controller;
		initLayout();
		initActions();
	}

	private void initActions() {

		loginButton.addActionListener(this::doLogin);
		
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
     }  
	
	
	private void initLayout() {
		
		JLabel label;
		
		setLayout(new GridBagLayout());
		
		setBackground(Color.WHITE);
		
        GridBagConstraints c;

        c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;
        
        try(InputStream is = getClass().getResourceAsStream("/img/estacio.png")){
        	
        	BufferedImage image = ImageIO.read(is);
        	
        	label = new JLabel();
        	
        	label.setHorizontalAlignment(SwingConstants.CENTER);
        	
        	label.setIcon(new ImageIcon(image));
        	
        	add(label, c);

        }catch(Exception e) {/**/}

        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;

        label = new JLabel(controller.getMessage("app.name"), SwingConstants.CENTER);
        
        label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
        
        add(label, c);

        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 2;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;

        label = new JLabel(controller.getMessage("app.work"), SwingConstants.CENTER);
        
        label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
        
        add(label, c);
        
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 3;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;

        label = new JLabel(controller.getMessage("app.programmer"), SwingConstants.CENTER);
        
        label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
        
        add(label, c);
        
        
        
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 4;
        c.fill = GridBagConstraints.HORIZONTAL;
        
        label = new JLabel(controller.getMessage("login.user-label")); 
        
        label.setHorizontalAlignment(Label.RIGHT);
        
        add(label, c);

        
        this.userField = new JTextField("", FIELDS_SIZE);
        
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 4;
        c.fill = GridBagConstraints.HORIZONTAL;
        
        add(this.userField, c);
        
        
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 5;
        c.fill = GridBagConstraints.HORIZONTAL;
        
        label = new JLabel(controller.getMessage("login.pass-label"));
        
        label.setHorizontalAlignment(SwingConstants.RIGHT);
        
        add(label, c);        
        
        
        this.passField = new JPasswordField("", FIELDS_SIZE);
        
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 5;
        c.fill = GridBagConstraints.HORIZONTAL;
        
        add(this.passField, c);
        
        this.loginButton = new JButton(controller.getMessage("login.signin"));
        
        c = new GridBagConstraints();
        
        c.gridx = 0;
        c.gridy = 6;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;
        		
        
        this.loginButton.setBackground(Color.WHITE);
        
        add(this.loginButton, c);
		
	}

	private void doValidate(){
		
		if(this.userField.getText().isBlank() || this.passField.getPassword() == null || this.passField.getPassword().length == 0)
			throw new SecurityException(controller.getMessage("login.validation.message"));
	}
	
	
	private void doLogin(ActionEvent evt) {
		
		try {
			
			doValidate();
			
			LoginModel model = new LoginModel();
			
			model.setLogin(this.userField.getText());
			
			model.setPassword(new String(this.passField.getPassword()));
			
			controller.doLogin(model);

		}catch(Exception e) {
			
			controller.showError(controller.getMessage("app.auth.error"), e.getMessage());
			
		}
		
	}
	
}
