package br.dev.brunocesar.estacio.osapp.controller;

import java.util.List;

import br.dev.brunocesar.estacio.osapp.context.ApplicationContext;
import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;
import br.dev.brunocesar.estacio.osapp.context.ViewContext;
import br.dev.brunocesar.estacio.osapp.entity.OrdemServico;
import br.dev.brunocesar.estacio.osapp.model.OrdemServicoTableModel;
import br.dev.brunocesar.estacio.osapp.repo.OrdemServicoRepository;
import br.dev.brunocesar.estacio.osapp.status.SituacaoOrdemServico;

public class OrdemServicoController extends Controller{

	public OrdemServicoController(ViewContext viewContext, PersistenceContext persistenceContext, ApplicationContext appContext) {
		super(viewContext, persistenceContext, appContext);
	}
	
	private OrdemServicoTableModel getOrdensServicoTableModel(SituacaoOrdemServico situacao) {
		
		OrdemServicoRepository ordemRepo = new OrdemServicoRepository(getPersistenceContext().getEntityManager());
		
		List<OrdemServico> ordens = ordemRepo.getOrdensListByLoginAndSituacao(getAppContext().getCurrentLogin(), situacao);
		
		return new OrdemServicoTableModel(ordens);

	}
	
	public OrdemServicoTableModel getOrdensAtivasTableModel() {
		return getOrdensServicoTableModel(SituacaoOrdemServico.ATIVA);
	}

	public OrdemServicoTableModel getOrdensFinalizadasTableModel() {
		return getOrdensServicoTableModel(SituacaoOrdemServico.FINALIZADA);
	}
	
	public void finalizar(Long numero) {
		new OrdemServicoRepository(getPersistenceContext().getEntityManager()).atualizarSituacao(numero, SituacaoOrdemServico.FINALIZADA);
	}

	public void reabrir(Long numero) {
		new OrdemServicoRepository(getPersistenceContext().getEntityManager()).atualizarSituacao(numero, SituacaoOrdemServico.ATIVA);
	}

}
