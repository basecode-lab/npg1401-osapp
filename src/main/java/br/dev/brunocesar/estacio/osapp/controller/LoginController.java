package br.dev.brunocesar.estacio.osapp.controller;

import org.apache.commons.lang3.StringUtils;

import br.dev.brunocesar.estacio.osapp.context.ApplicationContext;
import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;
import br.dev.brunocesar.estacio.osapp.context.ViewContext;
import br.dev.brunocesar.estacio.osapp.entity.Usuario;
import br.dev.brunocesar.estacio.osapp.model.LoginModel;
import br.dev.brunocesar.estacio.osapp.repo.UsuarioRepository;

public class LoginController extends Controller{

	public LoginController(ViewContext viewContext, PersistenceContext persistenceContext, ApplicationContext applicationContext) {
		super(viewContext, persistenceContext, applicationContext);
	}

	public void doLogin(LoginModel model) {
		
		if(StringUtils.isAnyEmpty(model.getLogin(), model.getPassword())) {
			throw new SecurityException(getMessage("login.validation.message"));
		}
		
		UsuarioRepository usuarioRepo = new UsuarioRepository(getPersistenceContext().getEntityManager());
		
		Usuario usuario = usuarioRepo.getByIdAndPassword(model.getLogin(), getPersistenceContext().hash(model.getPassword()));
		
		if(usuario == null) throw new SecurityException(getMessage("login.failed"));
		
		getAppContext().setCurrentLogin(usuario.getLogin());
		
		getViewContext().openOrdemServicoView();
		
	}
	
}