package br.dev.brunocesar.estacio.osapp.repo;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.dev.brunocesar.estacio.osapp.entity.OrdemServico;
import br.dev.brunocesar.estacio.osapp.status.SituacaoOrdemServico;

public class OrdemServicoRepository extends AbstractRepository{

	public OrdemServicoRepository(EntityManager entityManager) {
		super(entityManager);
	}
	
	
	public List<OrdemServico> getOrdensListByLoginAndSituacao(String login, SituacaoOrdemServico situacao){
		
		try {
			
			String sql = "from OrdemServico where login = :login and situacao = :situacao";
			
			TypedQuery<OrdemServico> query = getEntityManager().createQuery(sql, OrdemServico.class);
			
			query.setParameter("login", login);
			
			query.setParameter("situacao", situacao);
			
			return query.getResultList();

		}catch(Exception e) {
			
			return Collections.emptyList();
		}
		
	}
	
	public void atualizarSituacao(Long numero, SituacaoOrdemServico situacao) {
		
		String sql = "update OrdemServico set situacao = :situacao where numero = :numero";
		
		EntityTransaction tx = getEntityManager().getTransaction();
		
		Query query = getEntityManager().createQuery(sql);
		
		query.setParameter("situacao", situacao);
		
		query.setParameter("numero", numero);
		
		tx.begin();
		
		query.executeUpdate();
		
		tx.commit();
		
	}

}
