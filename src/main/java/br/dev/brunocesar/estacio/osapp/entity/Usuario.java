package br.dev.brunocesar.estacio.osapp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;

@Entity(name="Usuario")
@Table(schema=PersistenceContext.DB, name="usuario")
public class Usuario implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ds_login")
	private String login;
	
	@Column(name="ds_nome")
	private String nome;

	@Id
	@Column(name="vl_senha")
	private String senha;
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
