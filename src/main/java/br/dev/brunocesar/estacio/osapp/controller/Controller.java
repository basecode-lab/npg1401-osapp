package br.dev.brunocesar.estacio.osapp.controller;

import br.dev.brunocesar.estacio.osapp.context.ApplicationContext;
import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;
import br.dev.brunocesar.estacio.osapp.context.ViewContext;

public abstract class Controller {
	
	private final ViewContext viewContext;
	
	private final PersistenceContext persistenceContext;

	private final ApplicationContext appContext;
	
	public Controller(ViewContext viewContext, PersistenceContext persistenceContext, ApplicationContext appContext) {
		this.viewContext = viewContext;
		this.persistenceContext = persistenceContext;
		this.appContext = appContext;
	}

	public ViewContext getViewContext() {
		return viewContext;
	}
	
	public PersistenceContext getPersistenceContext() {
		return persistenceContext;
	}
	
	public void showError(String title, String message) {
		this.viewContext.showErrorMessage(null, title, message);
	}
	
	public void showSuccess(String title, String message) {
		this.viewContext.showInfoMessage(null, title, message);
	}
	
	public ApplicationContext getAppContext() {
		return appContext;
	}
	
	public String getMessage(String key) {
		return appContext.getMessage(key);
	}

}