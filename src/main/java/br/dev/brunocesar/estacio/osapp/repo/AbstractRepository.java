package br.dev.brunocesar.estacio.osapp.repo;

import javax.persistence.EntityManager;

public abstract class AbstractRepository {
	
	private final EntityManager entityManager;

	public AbstractRepository(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
}
