package br.dev.brunocesar.estacio.osapp.infra;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.commons.io.IOUtils;

import br.dev.brunocesar.estacio.osapp.context.PersistenceContext;
import br.dev.brunocesar.estacio.osapp.controller.MainController;

public class DatabaseInstaller {

	private static final String INSTALLER_SCRIPT = "/db/install.sql";

	private PersistenceContext persistenceContext;

	public DatabaseInstaller(PersistenceContext persistenceContext) {
		super();
		this.persistenceContext = persistenceContext;
	}

	public void install() throws IOException {

		final StringBuilder statements = new StringBuilder();

		try(InputStream resourceAsStream = MainController.class.getResourceAsStream(INSTALLER_SCRIPT)){
			IOUtils.readLines(resourceAsStream, StandardCharsets.UTF_8).forEach(statements::append);
		}

		EntityManager em = persistenceContext.getEntityManager();

		EntityTransaction tx = em.getTransaction();

		tx.begin();

		em.createNativeQuery(statements.toString()).executeUpdate();

		tx.commit();

		em.close();

	}

}