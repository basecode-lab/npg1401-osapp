package br.dev.brunocesar.estacio.osapp.context;

import java.util.Properties;

public class ApplicationContext {

	private String currentLogin;

	private Properties messages;
	
	public void setCurrentLogin(String currentLogin) {
		this.currentLogin = currentLogin;
	}

	public String getCurrentLogin() {
		return currentLogin;
	}
	
	public void setMessages(Properties messages) {
		this.messages = messages;
	}
	
	public Properties getMessages() {
		return messages;
	}
	
	public String getMessage(String key) {
		return messages.getProperty(key);
	}
	
}
