package br.dev.brunocesar.estacio.osapp.status;

public enum SituacaoOrdemServico {

	ATIVA("Ativa"),
	FINALIZADA("Finalizada");
	
	private String descricao;
	
	private SituacaoOrdemServico(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
