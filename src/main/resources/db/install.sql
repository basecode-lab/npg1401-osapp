create schema npg1401_osapp;

CREATE TABLE NPG1401_OSAPP.usuario(
  ds_login varchar(20) NOT NULL PRIMARY KEY,
  ds_nome varchar(50) NOT NULL,
  vl_senha text NOT null
);

CREATE TABLE NPG1401_OSAPP.cliente
(id_cliente bigint NOT NULL PRIMARY KEY auto_increment,
 no_cliente varchar(100) NOT NULL,
 nu_telefone varchar(9) NOT NULL,
 dt_cadastro timestamp NOT NULL
);

create TABLE NPG1401_OSAPP.ordem_servico
(nu_ordem_servico bigint NOT NULL PRIMARY key auto_increment,
 dt_cadastro timestamp NOT NULL,
 ds_login varchar(20) NOT NULL,
 id_cliente bigint NOT NULL,
 tx_ordem_servico text NOT NULL,
 st_ordem_servico tinyint NOT NULL
);

alter TABLE NPG1401_OSAPP.ordem_servico ADD CONSTRAINT fk_os_usuario FOREIGN key (ds_login) REFERENCES NPG1401_OSAPP.usuario(ds_login);
alter TABLE NPG1401_OSAPP.ordem_servico ADD CONSTRAINT fk_os_cliente FOREIGN key (id_cliente) REFERENCES NPG1401_OSAPP.cliente(id_cliente);

INSERT INTO NPG1401_OSAPP.usuario(ds_login, ds_nome,vl_senha) values('bruno','Bruno Cesar',HASH('SHA256', STRINGTOUTF8('estacio'), 1));
INSERT INTO NPG1401_OSAPP.usuario(ds_login, ds_nome,vl_senha) values('estacio','C.Univ. Estacio de Sa',HASH('SHA256', STRINGTOUTF8('estacio'), 1));

INSERT INTO NPG1401_OSAPP.cliente(no_cliente, nu_telefone, dt_cadastro) values('Joao da Silva','988887777',current_date());
INSERT INTO NPG1401_OSAPP.cliente(no_cliente, nu_telefone, dt_cadastro) values('Maria Joaquina','966665555',current_date());
INSERT INTO NPG1401_OSAPP.cliente(no_cliente, nu_telefone, dt_cadastro) values('Marcela Augusta','944443333',current_date());
INSERT INTO NPG1401_OSAPP.cliente(no_cliente, nu_telefone, dt_cadastro) values('Pedro Duarte','933332222',current_date());

INSERT INTO NPG1401_OSAPP.ORDEM_SERVICO (dt_cadastro, ds_login,ID_CLIENTE, tx_ordem_servico, st_ordem_servico) VALUES 
(current_timestamp(), 'bruno',1,'Consertar geladeira',0),
(current_timestamp(), 'estacio',2,'Consertar prateleira',0),
(current_timestamp(), 'bruno',3,'Confeccionar mala',1),
(current_timestamp(), 'estacio',4,'Trocar memoria do computador',0),
(current_timestamp(), 'estacio',1,'Trocar tela do celular',0),
(current_timestamp(), 'bruno',2,'Consertar placa mae',0),
(current_timestamp(), 'estacio',3,'Montar computador',0),
(current_timestamp(), 'bruno',4,'Trocar teclado',1);
